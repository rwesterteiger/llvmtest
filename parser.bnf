/* Lexical part */

_digit : '0'-'9' ;
_alpha : 'a'-'z' ;
_alnum : _digit | _alpha ;

int64 : _digit {_digit} ;

identifier : _alpha {_alnum} ;

!whitespace : ' ' | '\t' | '\n' | '\r' ;


/* Syntax part */

<< 
import(
	"bitbucket.org/rwesterteiger/llvmtest/ast"
)

>>

Calc : FuncList; 

FuncList :
		Func 			<< ast.NewFuncListNode($0),		nil >>
	|	FuncList Func   << ast.AppendFunc($0, $1),  	nil >>
;

Func :
		"def" identifier "(" ArgList ")" "{" StmtList "}"	<< ast.NewFuncNode($1, $3, $6), nil >>
;

ArgList :
		empty					// will return nil
	|	identifier      		<< ast.NewArgListNode($0), 		nil >>
	|	ArgList "," identifier 	<< ast.AppendArg($0, $2),  		nil >>
;

ExprList :
		empty					// will return nil
	|	Expr      				<< ast.NewExprListNode($0), 		nil >>
	|	ExprList "," Expr  		<< ast.AppendExpr($0, $2),  		nil >>
;
StmtList :
		Stmt  			<< ast.NewStatementListNode($0), nil >>
	|	StmtList Stmt   << ast.AppendStatement($0, $1) , nil >>
;

Stmt :
		LValue "=" Compare	";" 									<< ast.NewAssignNode($0, $2),  				nil >>
	|	"if"    Compare "{" StmtList "}"							<< ast.NewIfNode($1, $3, nil), 				nil >>
	|	"if"	Compare "{" StmtList "}" "else" "{" StmtList "}" 	<< ast.NewIfNode($1, $3, $7),  				nil >>
	|	"while" Compare "{" StmtList "}" 							<< ast.NewWhileNode($1, $3),   				nil >>
	|   "print" Compare ";" 										<< ast.NewPrintNode($1),       				nil >>
	|   "break" ";"													<< ast.NewBreakNode(),         				nil >>
	|   "return" Expr ";"											<< ast.NewReturnNode($1),					nil >>
	|	Expr ";"													<< ast.NewIgnoreExpressionValueNode($0), 	nil >>
;

/*
LValue :
		"a[" Expr "]"  << ast.NewArgRefNode($1), nil >>
;

*/

LValue :
		identifier  << ast.NewArgRefNode($0), nil >>
;

Compare :
		Expr "==" Expr   << ast.NewCompareNode($0, $2, $1), nil >>
	|	Expr "!=" Expr   << ast.NewCompareNode($0, $2, $1), nil >>
	|	Expr "<"  Expr   << ast.NewCompareNode($0, $2, $1), nil >>
	|	Expr ">"  Expr   << ast.NewCompareNode($0, $2, $1), nil >>
	|	Expr
;

Expr :
		Expr "+" Term	<< ast.NewBinOpNode($0, $2, $1),  nil >>
	|	Expr "-" Term	<< ast.NewBinOpNode($0, $2, $1), nil >>
	|	Term			
;

Term :
		Term "*" Factor	<< ast.NewBinOpNode($0, $2, $1), nil >>
	|	Term "/" Factor << ast.NewBinOpNode($0, $2, $1), nil >>
	|	Factor			
;

Factor :
		"(" Expr ")"	<< $1, nil >>
	|	Value
	|	"-" Value		<< ast.NewUnaryMinusNode($1), nil >>
;

Value :
		LValue
	|	identifier "(" ExprList ")" << ast.NewCallNode($0, $2), nil >>
	|	int64						<< ast.NewLiteralNode($0) >>
;