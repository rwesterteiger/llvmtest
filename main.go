// Taken from: http://npcontemplation.blogspot.com/2008/06/secret-of-llvm-c-bindings.html
package main

import (
	"os"
	"fmt"
	"io/ioutil"
	"strconv"
	"unsafe"
	"llvm.org/llvm/bindings/go/llvm"
	"bitbucket.org/rwesterteiger/llvmtest/lexer"
	"bitbucket.org/rwesterteiger/llvmtest/parser"

	"bitbucket.org/rwesterteiger/llvmtest/ast"
)


func initLLVM() {
	llvm.LinkInMCJIT()
	llvm.InitializeNativeTarget()
	llvm.InitializeNativeAsmPrinter()
}

func makeModule(prog ast.Program, args []int64) llvm.Module {
	mod := llvm.NewModule("main_module") // don't Dispose() because executionengine takes ownership

	ptrType := llvm.PointerType(llvm.Int64Type(), 0)

	ptrInt := llvm.ConstInt(llvm.Int64Type(), uint64(uintptr(unsafe.Pointer(&(args[0])))), false)
	ptrValue := llvm.ConstIntToPtr(ptrInt, ptrType)

	glob := llvm.AddGlobal(mod, ptrType, "args")
	glob.SetInitializer(ptrValue)
	glob.SetGlobalConstant(true)

	prog.GenCode(mod)

	return mod
}

func runPasses(engine llvm.ExecutionEngine, mod llvm.Module) {
	pass := llvm.NewPassManager()
	defer pass.Dispose()

	pass.Add(engine.TargetData())

	pass.AddConstantPropagationPass()

	pass.AddScalarReplAggregatesPass()
	pass.AddPromoteMemoryToRegisterPass()
	pass.AddInstructionCombiningPass()
	pass.AddDeadStoreEliminationPass()
	pass.AddReassociatePass()
	pass.AddGVNPass()
	pass.AddCFGSimplificationPass()

	pass.Run(mod)
}

func makeExecutionEngine(mod llvm.Module) llvm.ExecutionEngine {
	opts := llvm.NewMCJITCompilerOptions()

	opts.SetMCJITOptimizationLevel(4)
    opts.SetMCJITEnableFastISel(false)
    opts.SetMCJITNoFramePointerElim(true)
    opts.SetMCJITCodeModel(llvm.CodeModelJITDefault)

	engine, err := llvm.NewMCJITCompiler(mod, opts)

	if err != nil {
		panic(err)
	}

	return engine
}

func compileModule(mod llvm.Module) llvm.ExecutionEngine {
	err := llvm.VerifyModule(mod, llvm.ReturnStatusAction)
	if err != nil {
		panic(err)
	}

	engine := makeExecutionEngine(mod)

	runPasses(engine, mod)
	mod.Dump()

	return engine
}

func callFunction(engine llvm.ExecutionEngine, mod llvm.Module, f llvm.Value)  {
	exec_args := []llvm.GenericValue{}
	engine.RunFunction(f, exec_args)
}

func parseProgram(fileName string) ast.Program {
	p := parser.NewParser()

	// src := "7 * (1+5)"
	src, err := ioutil.ReadFile(fileName)

	if err != nil {
		panic(err)
	}
	// src = "while a[0] { a[0] = a[0]*2; print a[0]; a[1] = a[1] + 1; }"
	fmt.Printf("Compiling:\n%v\n", string(src))

	l := lexer.NewLexer([]byte(src))
	root, err := p.Parse(l)

	if err != nil {
		panic(err)
	}

	return root.(ast.Program)
}

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("Syntax: %s <expression> <arg0> [...]\n", os.Args[0])
		return
	}

	args := make([]int64, 8, 8)

	for i,s := range os.Args[2:] {
		x, err := strconv.ParseInt(s, 10, 64)

		if err != nil {
			panic(err)
		}
		args[i] = x
	}

	initLLVM()
	
	root := parseProgram(os.Args[1])
	mod  := makeModule(root, args)

	engine := compileModule(mod)
	defer engine.Dispose()


	mainFunc := mod.NamedFunction("main")

	if mainFunc.IsNil() {
		panic("Function main() not found!")
	}

	callFunction(engine, mod, mainFunc)
}
