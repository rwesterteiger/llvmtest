package ast

import (
	"log"
	"llvm.org/llvm/bindings/go/llvm"
	"bitbucket.org/rwesterteiger/llvmtest/token"
	//"bitbucket.org/rwesterteiger/llvmtest/util"
	//"bitbucket.org/rwesterteiger/llvmtest/runtime"
)

type funcNode struct {
	id string
	args []string // simply the list of names of the arguments
	stmts statementListNode
}

type funcListNode []*funcNode

func NewFuncListNode(f interface{}) funcListNode {
	return AppendFunc(funcListNode{}, f)
}

func AppendFunc(fl, f interface{}) funcListNode {
	return append(fl.(funcListNode), f.(*funcNode))
}

// implement interface Program
func (fl funcListNode) GenCode(mod llvm.Module) {
	for _, f := range fl {
		f.genCode(mod)
	}
}

func NewFuncNode(id, args, sl interface{}) *funcNode {
	if args == nil {
		args = argListNode{}
	}
	// fmt.Printf("new func %v, args = %v (type %T)\n", string(id.(*token.Token).Lit), args, args)
	return &funcNode{ string(id.(*token.Token).Lit), args.(argListNode), sl.(statementListNode) }
}

func (n *funcNode) genCode(mod llvm.Module) {
	argTypes := []llvm.Type{}

	for _ = range n.args {
		argTypes = append(argTypes, llvm.Int64Type())
	}

	// main_type := llvm.FunctionType(llvm.Int64Type(), main_args, true) // returns signed int64
	funcType := llvm.FunctionType(llvm.Int64Type(), argTypes, true)

	f := llvm.AddFunction(mod, n.id, funcType)
	f.SetFunctionCallConv(llvm.CCallConv)

	entry := llvm.AddBasicBlock(f, "entry")

	builder := llvm.NewBuilder()
	defer builder.Dispose()

	builder.SetInsertPointAtEnd(entry)
	// scratch := builder.CreateArrayAlloca(llvm.Int64Type(), llvm.ConstInt(llvm.Int64Type(), 1024, false), "scratch")


	varBindings := make(map[string]llvm.Value) // maps variable names to their stack locations

	// create space for argument copies
	for _, name := range n.args {
		_, hasKey := varBindings[name]

		if hasKey {
			log.Panicf("Duplicate argument name '%v' in function '%v'!", name, n.id)
		}

		varBindings[name] = builder.CreateAlloca(llvm.Int64Type(), name)
	}

	// copy the arguments over
	for i, name := range n.args {
		builder.CreateStore(f.Param(i), varBindings[name])
	}

	codeGenContext := newCodeGenContext(mod, builder, f, varBindings)

	if !n.stmts.genCode(codeGenContext) {
		// fmt.Printf("creating return 0 for function %v\n", n.id)
		builder.CreateRet(llvm.ConstInt(llvm.Int64Type(), 0, true)) // return 0 if end of function reached without 'return' statement
	}

	// mod.Dump()
}

type argListNode []string

func NewArgListNode(a interface{}) argListNode {
	return AppendArg(argListNode{}, a)
}

func AppendArg(al, a interface{}) argListNode {
	return append(al.(argListNode), string(a.(*token.Token).Lit))
}

