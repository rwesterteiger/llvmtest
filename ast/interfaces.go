package ast

import (
	"llvm.org/llvm/bindings/go/llvm"
)

type Program interface {
	GenCode(mod llvm.Module)
}

type statementNode interface {
	// returns true if the statement was a return, terminating the current block
	// the code generation for while & if needs to know this and will then suppress the following branch
	// if they dont do this llvm will throw an assertion because of a terminator in the middle of a block
	genCode(ctx *codeGenContext) bool
}

type expressionNode interface {
	genEvalCode(ctx *codeGenContext) llvm.Value
}

type lValueNode interface {
	expressionNode
	genAssignCode(ctx *codeGenContext, v llvm.Value)
}


type statementListNode []statementNode // used for statement blocks, e.g. body of a function or a while loop
type exprListNode []expressionNode // used in function call arguments
