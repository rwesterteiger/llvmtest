package ast

import (
	"llvm.org/llvm/bindings/go/llvm"
	"bitbucket.org/rwesterteiger/llvmtest/token"
	"log"
)


type whileNode struct {
	condition expressionNode
	body statementListNode
}

func NewWhileNode(condition, body interface{}) statementNode {
	return &whileNode{ condition.(expressionNode), body.(statementListNode) }
}

func (n *whileNode) genCode(ctx *codeGenContext) bool {
	condBlock := llvm.AddBasicBlock(ctx.f, "while_cond")
	bodyBlock := llvm.AddBasicBlock(ctx.f, "while_body")
	doneBlock := llvm.AddBasicBlock(ctx.f, "while_done")

	// condition block
	ctx.b.CreateBr(condBlock) // previous block always needs to end with a branch - no fall-through. so branch to condition block explicitly

	ctx.b.SetInsertPointAtEnd(condBlock)
	condExpressionValue := n.condition.genEvalCode(ctx)
	cmp := ctx.b.CreateICmp(llvm.IntEQ, condExpressionValue, llvm.ConstInt(llvm.Int64Type(), 0, true), "cmptmp") // compare with 0
	ctx.b.CreateCondBr(cmp, doneBlock, bodyBlock)

	// body block
	ctx.b.SetInsertPointAtEnd(bodyBlock)
	if !n.body.genCode(ctx) { // if block was not terminated by a return statement...
		ctx.b.CreateBr(condBlock) // jump back to condition block after body
	}

	// done block
	ctx.b.SetInsertPointAtEnd(doneBlock)

	return false
}


type ifNode struct {
	condition expressionNode
	thenBody, elseBody statementListNode
}

// elseBody can be nil
func NewIfNode(condition, thenBody, elseBody interface{}) statementNode {
	n := &ifNode{  condition : condition.(expressionNode), thenBody : thenBody.(statementListNode) }
	if elseBody != nil {
		n.elseBody = elseBody.(statementListNode)
	}
	return n
}

func (n *ifNode) genCode(ctx *codeGenContext) bool {
	condBlock := llvm.AddBasicBlock(ctx.f, "if_cond")
	thenBlock := llvm.AddBasicBlock(ctx.f, "if_then")
	elseBlock := llvm.AddBasicBlock(ctx.f, "if_else")
	doneBlock := llvm.AddBasicBlock(ctx.f, "if_done")

	// condition block
	ctx.b.CreateBr(condBlock) // previous block always needs to end with a branch - no fall-through. so branch to condition block explicitly

	ctx.b.SetInsertPointAtEnd(condBlock)
	condExpressionValue := n.condition.genEvalCode(ctx)
	cmp := ctx.b.CreateICmp(llvm.IntEQ, condExpressionValue, llvm.ConstInt(llvm.Int64Type(), 0, true), "cmptmp") // compare with 0
	ctx.b.CreateCondBr(cmp, elseBlock, thenBlock)

	// then block
	ctx.b.SetInsertPointAtEnd(thenBlock)
	if !n.thenBody.genCode(ctx) {
		ctx.b.CreateBr(doneBlock)
	}

	// else block
	ctx.b.SetInsertPointAtEnd(elseBlock)
	if !n.elseBody.genCode(ctx) {
		ctx.b.CreateBr(doneBlock)
	}

	// done block
	ctx.b.SetInsertPointAtEnd(doneBlock)

	return false
}

type returnNode struct {
	arg expressionNode
}

func NewReturnNode(arg interface{}) statementNode {
	return &returnNode{ arg.(expressionNode) }
}

func (n *returnNode) genCode(ctx *codeGenContext) bool {
	ctx.b.CreateRet(n.arg.genEvalCode(ctx))
	return true
}


type callNode struct {
	id string
	args exprListNode
}

func NewCallNode(id, args interface{}) expressionNode {
	if args == nil {
		args = exprListNode{}
	}

	return &callNode{ string(id.(*token.Token).Lit), args.(exprListNode) }
}

func (n *callNode) genEvalCode(ctx *codeGenContext) llvm.Value {
	f := ctx.mod.NamedFunction(n.id)

	if f.IsNil() {
		log.Panicf("Unknown function name '%v' in call!", n.id)
	}

	args := []llvm.Value{}
	for _, a := range n.args {
		args = append(args, a.genEvalCode(ctx))
	}

	return ctx.b.CreateCall(f, args, "func_result")
}