package ast

import (
	"log"
	"llvm.org/llvm/bindings/go/llvm"
	"bitbucket.org/rwesterteiger/llvmtest/token"
)



func NewCompareNode(left, right, tok interface{}) expressionNode {
	n := &compareNode{ left : left.(expressionNode), right : right.(expressionNode) }

	switch string(tok.(*token.Token).Lit)  {
	case "<":
		n.pred = llvm.IntSLT;
	case ">":
		n.pred = llvm.IntSGT;
	case "==":
		n.pred = llvm.IntEQ;
	case "!=":
		n.pred = llvm.IntNE;
	default:
		log.Panicf("Unrecognized comparison operator!")
	}

	return n;
}

func NewBinOpNode(left, right, tok interface{}) expressionNode {
	return &binOpNode{ left.(expressionNode), right.(expressionNode), tok.(*token.Token).Lit[0] }
}


type compareNode struct {
	left, right expressionNode
	pred llvm.IntPredicate
}

func (n *compareNode) genEvalCode(ctx *codeGenContext) llvm.Value {
	cmp := ctx.b.CreateICmp(n.pred, n.left.genEvalCode(ctx), n.right.genEvalCode(ctx), "cmpresult")
	return ctx.b.CreateZExtOrBitCast(cmp, llvm.Int64Type(), "cmpresult_int64")
}

type binOpNode struct {
	left, right expressionNode
	op byte
}

func (n *binOpNode) genEvalCode(ctx *codeGenContext) llvm.Value {
	a,b := n.left.genEvalCode(ctx), n.right.genEvalCode(ctx)

	switch string(n.op) {
		case "+":
			return ctx.b.CreateAdd(a,b, "sum")
		case "-":
			return ctx.b.CreateSub(a,b, "diff")
		case "*":
			return ctx.b.CreateMul(a,b, "prod")
		case "/":
			return ctx.b.CreateSDiv(a,b, "div")
	}

	log.Panicf("Unrecognized binary op '%v'!", n.op)
	return llvm.Value{} // never reached
}


func NewUnaryMinusNode(arg interface{}) expressionNode {
	return &binOpNode { &literalNode{ 0 }, arg.(expressionNode), "-"[0] }
}
