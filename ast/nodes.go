package ast

import (
	"log"
	"bitbucket.org/rwesterteiger/llvmtest/util"
	"bitbucket.org/rwesterteiger/llvmtest/token"
	"bitbucket.org/rwesterteiger/llvmtest/runtime"
	"llvm.org/llvm/bindings/go/llvm"
)

type codeGenContext struct {
	mod llvm.Module
	b llvm.Builder
	f llvm.Value
	// scratch llvm.Value // scratch array
	varBindings map[string]llvm.Value
}

func newCodeGenContext(mod llvm.Module, b llvm.Builder, f llvm.Value, varBindings map[string]llvm.Value) *codeGenContext {
	return &codeGenContext{ mod, b, f, varBindings }
}


func NewStatementListNode(n interface{}) statementListNode {
	return []statementNode { n.(statementNode) }
}

func AppendStatement(sl, n interface{}) statementListNode {
	return append(sl.(statementListNode), n.(statementNode))
}

func (sl statementListNode) genCode(ctx *codeGenContext) bool {
	for _,n := range sl {
		if n.genCode(ctx) {
			return true;
		}

	}

	return false;
}

func NewExprListNode(n interface{}) exprListNode {
	return exprListNode { n.(expressionNode) }
}

func AppendExpr(el, e interface{}) exprListNode {
	return append(el.(exprListNode), e.(expressionNode))
}



type literalNode struct {
	x int64
}

func NewLiteralNode(tok interface{}) (*literalNode, error) {
	x, err := util.IntValue(tok.(*token.Token).Lit) 
	return &literalNode{ x }, err
}

func (n *literalNode) genEvalCode(ctx *codeGenContext) llvm.Value {
	return llvm.ConstInt(llvm.Int64Type(), uint64(n.x), true) // true means signed
}


type argRefNode struct {
	id string
}

func NewArgRefNode(id interface{}) lValueNode {
	return &argRefNode{ string(id.(*token.Token).Lit) }
}

func (n *argRefNode) getArgPtr(ctx *codeGenContext) llvm.Value {
	loc, ok := ctx.varBindings[n.id]

	if !ok {
		log.Panicf("Unknown variable '%v'!", n.id)
	}

	return loc
}

func (n *argRefNode) genEvalCode(ctx *codeGenContext) llvm.Value {
	arg := ctx.b.CreateLoad(n.getArgPtr(ctx), "argval")
	return arg
}

func (n *argRefNode) genAssignCode(ctx *codeGenContext, v llvm.Value) {
	ctx.b.CreateStore(v, n.getArgPtr(ctx))
}

type assignNode struct {
	left lValueNode
	right expressionNode
}

func NewAssignNode(left, right interface{}) statementNode {
	return &assignNode{ left.(lValueNode), right.(expressionNode) }
}

func (n *assignNode) genCode(ctx *codeGenContext) bool {
	n.left.genAssignCode(ctx, n.right.genEvalCode(ctx))
	return false
}


type builtinCallNode struct {
	addr uint64
	args []expressionNode
}


func NewPrintNode(arg interface{}) statementNode {
	return &builtinCallNode{
		addr : runtime.Print_ptr(),
		args : []expressionNode{ arg.(expressionNode) }}
}

func NewBreakNode() statementNode {
	return &builtinCallNode{ addr : runtime.Break_ptr() } // no args
}

func (n *builtinCallNode) genCode(ctx *codeGenContext) bool {
	argValues := []llvm.Value{}
	argTypes  := []llvm.Type{}

	for _,a := range n.args {
		argValues = append(argValues, a.genEvalCode(ctx))
		argTypes  = append(argTypes, llvm.Int64Type())
	}

	funcType := llvm.FunctionType(llvm.VoidType(), argTypes, false)
	ptrType  := llvm.PointerType(funcType, 0)

	ptrInt  := llvm.ConstInt(llvm.Int64Type(), n.addr, false)
	funcPtr := llvm.ConstIntToPtr(ptrInt, ptrType)

	ctx.b.CreateCall(funcPtr, argValues, "")

	return false
}

// evaluates expression and ignores the result (for when an expression is used as a statement, e.g. "a[0];")
type ignoreExpressionValueNode struct {
	expr expressionNode
}

func NewIgnoreExpressionValueNode(expr interface{}) statementNode {
	return &ignoreExpressionValueNode{ expr.(expressionNode) }
}

func (n *ignoreExpressionValueNode) genCode(ctx *codeGenContext) bool {
	n.expr.genEvalCode(ctx);
	return false
}

