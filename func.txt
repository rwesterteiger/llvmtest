def fac(n) {
	if n == 0 {
		return 1;
	} else {
		return n * fac(n-1);
	}
}

def main() {
	print fac(6);
	return 0;
}