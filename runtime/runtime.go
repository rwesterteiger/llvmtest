package runtime

// void print_func(signed long long x); // forward declaration
import "C"

import (
	"fmt"
	"unsafe"
)

//export print_func
func print_func(x int64) {
	fmt.Printf("%v\n", x)
}

func Print_ptr() uint64 {
	return uint64(uintptr(unsafe.Pointer(C.print_func)))
}

func Break_ptr() uint64 {
	str := C.CString("\xCC\xC3")
	// dont free the string!

	return uint64(uintptr(unsafe.Pointer(str)))
}